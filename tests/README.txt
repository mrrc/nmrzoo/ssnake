### ssNake test suites ###

##### Writing tests ######
Please write tests to be compatible with our pytest configuration.
See pytest.ini in root for pytest configuration info,
such as the pattern for files that are recognised as test files.

##### Running tests ######
To run all tests, run 'pytest tests' or just 'pytest' in root.