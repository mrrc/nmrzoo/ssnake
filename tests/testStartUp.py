#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2016 - 2024 Bas van Meerten and Wouter Franssen

# This file is part of ssNake.
#
# ssNake is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ssNake is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ssNake. If not, see <http://www.gnu.org/licenses/>.

import subprocess

def testStartInCLI():
    ssnake_process = subprocess.Popen(['python', 'src/ssNake.py'],
                                      stdout = subprocess.PIPE,
                                      stderr = subprocess.PIPE,
                                      universal_newlines = True)
    try:
        stdout, stderr = ssnake_process.communicate(timeout=10)
    except subprocess.TimeoutExpired:
        pass # Program had no errors within 10 seconds, so probably started succesfully.
    else:
        assert stderr == '', "ssNake raised an error during start-up."
    finally:
        ssnake_process.terminate()
        ssnake_process.wait()
