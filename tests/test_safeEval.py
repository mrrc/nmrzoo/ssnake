#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2025 - the ssNake project

# This file is part of ssNake.
#
# ssNake is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ssNake is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ssNake. If not, see <http://www.gnu.org/licenses/>.

from safeEval import safeEval
import numpy as npy


def assertEqual(inp, expect):
    try:
        assert inp == expect
    except ValueError:
        assert npy.array_equiv(inp, expect)


def _testTypeFIplus(inp, expect, **kwargs):
    assertEqual(safeEval(inp, Type='FI', **kwargs), expect)
    assertEqual(safeEval(inp, Type='C', **kwargs), expect)
    assertEqual(safeEval(inp, Type='ArrFI', **kwargs), None if expect is None else [expect])
    assertEqual(safeEval(inp, Type='ArrC', **kwargs), None if expect is None else [expect])
    assertEqual(safeEval(inp, Type='All', **kwargs), expect)

def _testTypeCplus(inp, expect, **kwargs):
    assertEqual(safeEval(inp, Type='FI', **kwargs), None)
    assertEqual(safeEval(inp, Type='C', **kwargs), expect)
    assertEqual(safeEval(inp, Type='ArrFI', **kwargs), None)
    assertEqual(safeEval(inp, Type='ArrC', **kwargs), None if expect is None else [expect])
    assertEqual(safeEval(inp, Type='All', **kwargs), expect)

def _testTypeArrFIplus(inp, expect, **kwargs):
    assertEqual(safeEval(inp, Type='FI', **kwargs), None)
    assertEqual(safeEval(inp, Type='C', **kwargs), None)
    assertEqual(safeEval(inp, Type='ArrFI', **kwargs), expect)
    assertEqual(safeEval(inp, Type='ArrC', **kwargs), expect)
    assertEqual(safeEval(inp, Type='All', **kwargs), expect)

def _testTypeArrCplus(inp, expect, **kwargs):
    assertEqual(safeEval(inp, Type='FI', **kwargs), None)
    assertEqual(safeEval(inp, Type='C', **kwargs), None)
    assertEqual(safeEval(inp, Type='ArrFI', **kwargs), None)
    assertEqual(safeEval(inp, Type='ArrC', **kwargs), expect)
    assertEqual(safeEval(inp, Type='All', **kwargs), expect)

def _testTypeAll(inp, expect, **kwargs):
    assertEqual(safeEval(inp, Type='FI', **kwargs), None)
    assertEqual(safeEval(inp, Type='C', **kwargs), None)
    assertEqual(safeEval(inp, Type='ArrFI', **kwargs), None)
    assertEqual(safeEval(inp, Type='ArrC', **kwargs), None)
    assertEqual(safeEval(inp, Type='All', **kwargs), expect)



def testNumberNonArray():
    _testTypeFIplus("22", 22)
    _testTypeFIplus("0", 0)
    _testTypeFIplus("33.33", 33.33)
    _testTypeFIplus("pi", npy.pi)
    _testTypeFIplus("quad(lambda x: x**3, 0, 2)[0]", 4)
    _testTypeFIplus("round(arctan(sqrt(2))*180/pi)", 55)
    #_testTypeFIplus("round(arctan(sqrt(2))*180/pi, 3)", 54.736) # numpy's round is overwritten by scipy.specials round...
    _testTypeFIplus("arange(0,10)[-1]", 9) # numpy's int64
    _testTypeFIplus("linspace(1,2)[0]", 1) # numpy's float64

    _testTypeCplus("3j + 3", 3+3j)


def testNumberArray():
    _testTypeArrFIplus("[1, 2.3]", [1, 2.3]),
    _testTypeArrFIplus("(1, 2.3)", (1, 2.3)),
    _testTypeArrFIplus("{1, 2.3}", {1, 2.3}),
    _testTypeArrFIplus("1,2, 3", (1, 2, 3)),
    _testTypeArrFIplus("linspace(1,2,5)", npy.array([1, 1.25, 1.5,  1.75, 2]))

    _testTypeArrCplus("[1, 2.3+4j]", [1, 2.3+4j]),


def testNonNumber():
    _testTypeAll("[1, 2.3, None]", [1, 2.3, None]),
    _testTypeAll("{321: 123}", {321: 123}),
    _testTypeAll("('Integral', 3, -1+4j, None)", ('Integral', 3, -1+4j, None))
    _testTypeAll("inf", npy.inf)


def testExtraArguments():
    _testTypeFIplus("3+length*2", 9, length=3)
    _testTypeFIplus("3+length*2", 9.2, extraAttr={"length": 3.1})
    _testTypeFIplus("y(x)", 8, extraAttr={"x": 4, "y": lambda z: z*2})


def testBadInput():
    _testTypeFIplus("hello", None)
    _testTypeFIplus("eval('33')", None)
    _testTypeFIplus("broken_code()", None)
    _testTypeFIplus("None", None)
    _testTypeFIplus("4/0", None)
    _testTypeFIplus("[3, 1/0]", None)
