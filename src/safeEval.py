#!/usr/bin/env python3

# Copyright 2016 - 2024 Bas van Meerten and Wouter Franssen

# This file is part of ssNake.
#
# ssNake is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ssNake is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ssNake. If not, see <http://www.gnu.org/licenses/>.

import re
import numpy as np
import scipy.integrate
import scipy.special
import hypercomplex as hc
from types import FunctionType
import numbers

def isNumericReal(val):
    # Real numeric type, including the special types of Numpy
    return isinstance(val, numbers.Real) and np.isfinite(val)

def isNumeric(val):
    # Complex or real numeric type, including the special types of Numpy
    return isinstance(val, numbers.Number) and np.isfinite(val)

def safeEval(inp, length=None, Type='All', extraAttr=None):
    """
    Creates a more restricted eval environment.
    Note that this method is still not acceptable to process strings from untrusted sources.

    Parameters
    ----------
    inp : str
        String to evaluate.
    length : int or float, optional
        The variable length will be set to this value.
        By default the variable length is not set.
    Type : string, optional
        Type of expected output. Any of:
        - 'All' (default) will return all parseable types except strings
        - 'FI' will return a finite, real number if possible
        - 'C' will return a finite real or complex number if possible
        - 'ArrFI' will return an iterable object containing only finite, real numbers if possible
        - 'ArrC' will return an iterable object containing only finite, real or complex numbers if possible
    extraAttr : dict, optional
        A dictionary containing extra variables or functions accessible during the evaluation of the input inp
        For example, {"x": 22, "y": lambda x: x*2} adds the variable 'x' with value of 22 and the function y

    Returns
    -------
    Object
        The result of the evaluated string.
    None
        In case the string cannot be parsed as an acceptable data type
    """
    env = vars(np).copy()
    env.update(vars(hc).copy())
    env.update(vars(scipy.special).copy())
    env.update(vars(scipy.integrate).copy())

    allowedTypes = [
      FunctionType,
      float, int,
      np.ufunc,
      type(np.linspace) # np._ArrayFunctionDispatcher rather than function in np >= 1.25
    ]
    if hasattr(scipy.special, "legendre_p"):
        # Added in SciPy 1.15, class scipy.special._multiufuncs.MultiUFunc
        # legendre_p is just an example from within this class of math functions
        allowedTypes.append(type(scipy.special.legendre_p))
    env = {k: v for k, v in env.items() if not k.startswith('_') and type(v) in allowedTypes}

    # Explicitly allowed functions, can be overwritten using extraAttr if desired
    env["slice"] = slice
    env["arange"] = np.arange
    env["zeros"] = np.zeros

    if extraAttr:
        env.update(extraAttr)
    if length is not None:
        env["length"] = length

    env["locals"] = None
    env["globals"] = None
    env["__name__"] = None
    env["__file__"] = None
    env["__builtins__"] = {'None': None, 'False': False, 'True': True}

    inp = re.sub('([0-9]+)[kK]', r'\g<1>*1024', str(inp))
    try:
        val = eval(inp, env)
        if isinstance(val, str):
            return None
        if Type == 'All':
            return val
        if Type == 'FI':
            if isNumericReal(val):
                return val
            return None
        if Type == 'C':
            if isNumeric(val):
                return val
            return None
        if Type in ('ArrFI', 'ArrC'):
            numCheckFn = isNumericReal if Type == 'ArrFI' else isNumeric
            if not hasattr(val, "__iter__") or hasattr(val, "get"):
                # Filter non-iterables or mapping-type iterables.
                # Numbers are converted to a list containing the number
                if numCheckFn(val):
                    return [val]
            elif all([numCheckFn(x) for x in val]):
                return val
            return None
    except Exception:
        return None
