# -*- coding: utf-8 -*-
"""
Script to facilitate editing the files involved in building ssNake or its installers for Windows.
First, the ssnake directory to be built is copied. Then the paths to this directory,
the pyinstaller*, etc., as well as the version code are edited in the various building files.
These paths and version code need to be set by the user, as indicated at the start
of the if __name__ == '__main__' section.

*:Do not forget to consult the README before configuring your python environment.

Created on Sat Jun 29 16:54:10 2024

@author: Jop W. Wolffs
"""

import shutil
from pathlib import Path

import re

def prepare_buildfiles(path_exebuilder: Path,
                       path_spec: Path,
                       path_installbuilder: Path,
                       path_nsi: Path,
                       path_ssnakedir: Path,
                       path_pyinstaller: Path,
                       path_scipylibs: Path,
                       path_src: Path,
                       VERSION: str,
                       inplace_all: bool = True):
    '''
    Replaces paths and names in the buildfiles with ones supplied by
    the user to match local file structures and version numbers.
    Use inplace_all = False to make copies of the files instead of inplace
    editing (for testing purposes).
    '''
    
    # Exebuilder: replace pyinstaller path
    if not sed_re(path_exebuilder,inplace=inplace_all,
                  pattern="^[^ ]*pyinstaller\\.exe",
                  repl=str(path_pyinstaller).replace("\\","\\\\"))[0]:
        raise BaseException("Failed to replace the pyinstaller path in the exebuilder.")
    # Spec file: replace src dir, scipy libs dir in pathex and version id
    if not sed_re(path_spec,inplace=inplace_all,
                  pattern="(?<=\')[^\']*(?=\')",
                  repl=str(path_src).replace("\\","\\\\\\\\")+"\\\\\\\\",
                  linematch="^basePath")[0]:
        raise BaseException("Failed to replace ssnake src path in the specfile.")
    if not sed_re(path_spec,inplace=inplace_all,
                  pattern="[^\']*scipy[^\']*[^_]lib[^\']*",
                  repl=str(path_scipylibs).replace("\\","\\\\\\\\"))[0]:
        raise BaseException("Failed to replace scipy lib path in the specfile.")
    sed_re(path_spec, inplace=inplace_all,
           pattern="v[0-9]+\\.[0-9]+[^\']*", repl=VERSION)
    # installmaker: replace dir for manuals
    if not sed_re(path_installbuilder,inplace=inplace_all,
                  pattern="(?<=copy )[^ ]*(?=\\\\ReferenceManual.pdf)",
                  repl=str(path_ssnakedir).replace("\\","\\\\"))[0]:
        raise BaseException("Failed to replace reference manual path in the installbuilder.")
    if not sed_re(path_installbuilder,inplace=inplace_all,
                  pattern="(?<=xcopy /E )[^ ]*(?=\\\\Tutorial)",
                  repl=str(path_ssnakedir).replace("\\","\\\\"))[0]:
        raise BaseException("Failed to replace tutorial path in the installbuilder.")
    # nsi: replace version id
    if not sed_re(path_nsi,inplace=inplace_all,
                  pattern="v[0-9]+\\.[0-9]+[^\"]*",repl=VERSION,
                  linematch="!define APPNAME_FULL")[0]:
        raise BaseException("Failed to replace version code in the nsi.")

def sed_re(path_file, pattern, repl, linematch: str = '',
           inplace: bool = False, path_out: Path = None):
    '''
    Perform the pure-Python equivalent of `sed` substitution: e.g.,
    `sed -e ''${linematchpattern}'s/'${pattern}'/'${repl}' "${path_file}" "${path_out}"`.
    
    Adapted from Cecil Cury @ stackoverflow
    
    Returns a bool of whether a match was actually found in the file, and
    the path to the edited file.
    '''
    # For efficiency, precompile the passed regular expression.
    pattern_compiled = re.compile(pattern)
    linematch_compiled = re.compile(linematch)
    
    # Convert path_file to Path object if it isnt already. Works for relative paths
    path_file = Path(path_file)
    
    # Instead of assuming inplace and using a tempfile as in Curry's original,
    # There is an implicit assumption of a new file (path_out) with a non-random name
    if path_out == None:
        path_out = path_file.parent / increment_filename(path_file.name, new_branch = True)
    while path_out.is_file():
        path_out = path_out.parent / increment_filename(path_out.name)

    # Binary writing imposes non-trivial encoding constraints trivially
    # resolved by switching to text writing.
    # In addition, python files are explicitely opened with utf-encoding
    if path_out.suffix == '.py':
        encoding = 'utf8'
    else:
        encoding = None
    match_found = False
    with open(path_out, mode='w',encoding=encoding) as of:
        with open(path_file,encoding=encoding) as src_file:
            for line in src_file:
                if re.search(linematch_compiled, line):
                    match_found = True
                    of.write(pattern_compiled.sub(repl, line))
                else:
                    of.write(line)

    # Copy permissions etc, replace original with outfile if inplace = true
    shutil.copystat(path_file, path_out)
    if inplace:
        shutil.move(path_out, path_file)
        
    if inplace:
        return match_found, path_file
    else:
        return match_found, path_out
    
def increment_filename(filename, separator = '_', digits = None, new_branch = False):
    '''
    Returns the full filename string, including extension, but with any
    trailing integer (after the last separator, before the extension) incremented.
    Allows for leading zeros in number.
    Appends a number if there is none at the end.
    Digits = 0 will give the minimum digits required
    '''
       
    filename_as_path = Path(filename)
    name, extension = filename_as_path.stem, filename_as_path.suffix
    
    name_split  = name.split(separator)
    name_last   = name_split[-1]
    
    # parsing last part of name and consequences thereof
    if name_last.isdigit() and not new_branch:
        number_old = int(name_last[:-1].lstrip('0') + name_last[-1])
        name       = name_split[:-1]
        if digits == None:
            digits = len(name_last)
    else:
        number_old = 0
        name       = name_split
        if digits == None:
            digits = 2
        
    number_new = str(number_old + 1)
    
    # leading zeroes
    N_0 = digits - len(number_new)
    if N_0 > 0:
        number_new = N_0 * '0' + number_new
    
    name_new     = separator.join(name) + separator + number_new
    filename_new = name_new + extension
    
    return filename_new

if __name__ == '__main__':
    
    ### PATHS AND VERSION
    # !!Edit these!!
    # Path to the ssNake directory (will be copied to build location)
    path_ssnakedir_master = Path("C:/Users/jwolffs/Programs_IO/gitlab_science_ru_jwolffs/ssnake")
    # Path to the builder, pyinstaller.exe, in the right python environment
    path_pyinstaller = Path("C:/Users/jwolffs/Programs_IO/Python/virtualenvs/ssNake_build_1d5_test/Scripts/pyinstaller.exe")
    # Path to additional scipy libraries, in the right python environment
    path_scipylibs = Path("C:/Users/jwolffs/Programs_IO/Python/virtualenvs/ssNake_build_1d5_test/Lib/site-packages/scipy.libs")
    # Version string (will be visible in program banner and Windows Registry)
    VERSION="v1.5test2410"
    # Files that will get modified (but don't need to be edited here)
    path_ssnakedir_copy = Path.cwd() / 'ssnake'
    path_src = path_ssnakedir_copy / "src"
    path_exebuilder = Path.cwd() / "buildssNake.bat"
    path_installbuilder = Path.cwd() / "makeInstaller.bat"
    path_spec = Path.cwd() / "ssNake.spec"
    path_nsi = Path.cwd() / "modern_ssNake.nsi"
    
    
    ### SCRIPT CONTROL
    # Turn off for regex bug testing
    inplace_all = True
    
    ### SCRIPT ACTIONS
    # Copy desired ssNake directory to a new place to edit and use for the build
    if path_ssnakedir_copy.exists():
        confirmed = {'Y': True, 'N': False}[input(
            "There is already a ssNake directory at the build location. Overwrite (Y/N)?: ").upper()]
        if confirmed:
            shutil.rmtree(path_ssnakedir_copy)
            shutil.copytree(path_ssnakedir_master,path_ssnakedir_copy)
        else:
            pass
    else:
        shutil.copytree(path_ssnakedir_master,path_ssnakedir_copy)
    # ssNake.py: replace version id and EXE flag (not sure about effect of latter)
    sed_re(path_src / "ssNake.py", inplace=inplace_all,
           pattern="(?<=\')[^\']*(?=\')", repl=VERSION,
           linematch="^VERSION")
    sed_re(path_src / "ssNake.py", inplace=inplace_all,
           pattern="False", repl="True",
           linematch="^EXE")
    # Edit paths etc in buildfiles
    prepare_buildfiles(path_exebuilder, path_spec, path_installbuilder, path_nsi, 
                       path_ssnakedir_copy, path_pyinstaller, path_scipylibs, path_src, 
                       VERSION, inplace_all = inplace_all)
    
    
    pass
