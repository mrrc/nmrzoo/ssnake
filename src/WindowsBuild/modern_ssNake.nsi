;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;General
  !define APPNAME "ssNake"
  !define APPNAME_FULL "${APPNAME} v1.5test2410"
  !define DESCRIPTION "NMR processing utility"
  !define INSTALLSIZE 236500
  ;Name and file
  Name "${APPNAME}"
  OutFile "../${APPNAME_FULL} Installer.exe"

  ;Default installation folder
  InstallDir "$LOCALAPPDATA\${APPNAME_FULL}"
  SetCompressor /SOLID lzma
  
  ;Request application privileges for Windows Vista
  RequestExecutionLevel user

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  !define MUI_ICON "${APPNAME}\_internal\Icons\logo.ico"
  !define MUI_UNICON "${APPNAME}\_internal\Icons\logo.ico"

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "gpl.rtf"
  ;!insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
   Var StartMenuFolder
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\${APPNAME_FULL}" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  !define MUI_STARTMENUPAGE_DEFAULTFOLDER "${APPNAME_FULL}"
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  !insertmacro MUI_PAGE_INSTFILES

    !define MUI_FINISHPAGE_NOAUTOCLOSE
    !define MUI_FINISHPAGE_RUN "$INSTDIR\${APPNAME}\${APPNAME}.exe"
    !define MUI_FINISHPAGE_RUN_TEXT "Start ${APPNAME_FULL}"
  !insertmacro MUI_PAGE_FINISH
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "Main Files"

  SetOutPath "$INSTDIR"
  file /r "${APPNAME}"
  ;Temporary fix for getting the manuals in the right place
  ;file /r "Tutorial"
  ;file "ReferenceManual.pdf"
  
  ;Store installation folder
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}" "DisplayName" "${APPNAME_FULL} - ${DESCRIPTION}"
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}" "InstallLocation" "$\"$INSTDIR$\""
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}" "DisplayIcon" "$\"$INSTDIR\${APPNAME}\_internal\Icons\logo.ico$\""
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}" "Publisher" "${APPNAME}"
  # There is no option for modifying or repairing the install
  WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}" "NoModify" 1
  WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}" "NoRepair" 1
  # Set the INSTALLSIZE constant (!defined at the top of this script) so Add/Remove Programs can accurately report the size
  WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}" "EstimatedSize" ${INSTALLSIZE}
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  SetOutPath "$INSTDIR\${APPNAME}"
  ;Temporary fix for getting the manuals in the right place
  file /r "Tutorial"
  file /r "ReferenceManual.pdf"
  
    !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
	CreateShortcut "$SMPROGRAMS\$StartMenuFolder\${APPNAME_FULL}.lnk" "$INSTDIR\${APPNAME}\${APPNAME}.exe"
    CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  
    !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd


;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;FILES

  Delete "$INSTDIR\Uninstall.exe"
  ;Temporary fix for getting the manuals in the right place
  RMDir /r "$INSTDIR\${APPNAME}\Tutorial"
  Delete "$INSTDIR\${APPNAME}\ReferenceManual.pdf"
  RMDir /r "$INSTDIR\${APPNAME}"
  RMDir "$INSTDIR"
  
  ;Registry
  DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME_FULL}"
  
  ; Remove Start Menu launcher
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\${APPNAME_FULL}.lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
  
  DeleteRegKey /ifempty HKCU "Software\${APPNAME_FULL}"

SectionEnd
